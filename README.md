[![ru](https://img.shields.io/badge/language-russian-red.svg)](https://gitlab.com/lpsztemp/chsvreadenergy/-/blob/main/README-ru.md)

The repository contains an implementation of a kernel mode Windows driver reading energy consumed by CPU by means of Intel RAPL model-specific registers.
This access to energy data is currently only possible from kernel code, hence the kernel-mode driver of this repository.

Additionally, the repository contains a demo project which uses the driver to evaluate energy consumption of CPUs running sequential and parallel randomizers of memory buffers.

# Compilation for Windows

To compile the driver, use a [Windows Driver Kit] of a version chosen with respect to the target Windows system.

The Visual Studio solution provides the following build configurations:

Configuration               | Target Windows
----------------------------|---------------------------------------
win8.1-debug/win8.1-release | 8.1 and Server 2012 R2
win10-debug/win10-release   | 10, 11, Server 2016, Server 2019 and Server 2022

Only the x86-64 CPU architecture is currently supported, and only Intel CPUs, therefore the only VS target platform available is "x64".

# Installation on Windows

The code of the repository uses default test certificate to sign the driver, therefore its installation on Windows needs the following.
* Disable Secure Boot in BIOS.
* In Windows terminal opened with the Administrator privileges run the following
	* ``bcdedit /debug on`` to enable the kernel debug mode;
	* ``bcdedit /set nointegritychecks off`` to disable integrity checks of the driver;
	* ``bcdedit /set testsigning on`` to allow for installation of drivers with test signature.
	* ``shutdown /r /t 0`` to reboot the computer
* After reboot, use ``devcon.exe`` to install, update or remove the driver. The program ``devcon.exe`` is a part of [Windows Driver Kit] and is normally located in ``<path to WDK>\Tools\x64`` where ``<path to WDK>`` is a directory with the Kit installation. For example, the default path to the Windows Driver Kit installation for Windows 10 is ``C:\Program Files (x86)\Windows Kits\10``, and the path to ``devcon.exe`` is thus ``C:\Program Files (x86)\Windows Kits\10\Tools\x64\devcon.exe``.  
The ``devcon.exe`` program is portable as it does not require installation, i.e. the sole ``devcon.exe`` file can be moved around to target computers and used for installation in every Windows operating system listed in the table above.  
Also, see [this post on StackExchange](https://superuser.com/a/1099688).  
Assume that ``<path-to-driver>`` is a directory in which the three files of the driver, i.e. ``chsvreadenergy.cat``, ``chsvreadenergy.inf`` and ``chsvreadenergy.sys`` are located. For instance, the driver built by default in the ``win10-release`` configuration, has these files located in ``<repo>\x64\win10-release``, where ``<repo>`` is a local path to this repository.
	* Use ``devcon.exe install <path-to-driver>\chsvreadenergy.inf Root\chsvreadenergy`` to install the driver.
	* Use ``devcon.exe update <path-to-driver>\chsvreadenergy.inf Root\chsvreadenergy`` to update existing installation with a new version.
	* Use ``devcon.exe remove Root\chsvreadenergy`` to delete driver installation.  
Note that these commands are sensitive to the slash character used. The ``devcon.exe`` program can fail if forward slash `/` is used in place of backslash `\`. Also note that after update and removal of the driver, system reboot might be required.

# Platforms tested with the driver

Currently, the driver has been tested in following configurations:
* Intel Xeon E5-2695 v2 (06_3EH), 2-CPU configuration, on Windows Server 2012 R2;
* Intel i9-10980XE (06_55H) on Windows 10 22H2 and Windows 11 22H2;
* Intel i7-7700HQ (06_9EH) on Windows 10 22H2;
* Intel i7-4700HQ (06_3CH) on Windows 8.1.

[Windows Driver Kit]: https://learn.microsoft.com/en-us/windows-hardware/drivers/other-wdk-downloads