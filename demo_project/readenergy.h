#pragma once

/*All energies returned as double values are expressed in joules*/
typedef unsigned long long energy_mark_t;
energy_mark_t energy_read_core(unsigned core_index);
double energy_diff_core(unsigned core_index, energy_mark_t en1, energy_mark_t en2);
void energy_read_all_cores(energy_mark_t* en);
void energy_diff_all_cores(double* diffs, const energy_mark_t* en1, const energy_mark_t* en2);
double energy_diff_average_all_cores(const energy_mark_t* en1, const energy_mark_t* en2);
double energy_diff_total_all_cores(const energy_mark_t* en1, const energy_mark_t* en2);
energy_mark_t energy_read_cpu();
double energy_diff_cpu(energy_mark_t en1, energy_mark_t en2);

#ifdef __cplusplus
#include <vector>
#include <thread>

class energy_meter_t {
	std::vector<energy_mark_t> marks;
public:
	energy_meter_t():marks(std::thread::hardware_concurrency()) {
		energy_read_all_cores(marks.data());
	}
	void reset() {
		energy_read_all_cores(marks.data());
	}
	double core_energy(std::size_t core_index) const {
		return energy_diff_core((unsigned) core_index, marks[core_index], energy_read_core((unsigned) core_index));
	}
	double average_energy() const {
		auto latest = std::vector<energy_mark_t>(std::thread::hardware_concurrency());
		energy_read_all_cores(latest.data());
		return energy_diff_average_all_cores(marks.data(), latest.data());
	}
	double total_energy() const {
		auto latest = std::vector<energy_mark_t>(std::thread::hardware_concurrency());
		energy_read_all_cores(latest.data());
		return energy_diff_total_all_cores(marks.data(), latest.data());
	}
};
#endif //__cplusplus
