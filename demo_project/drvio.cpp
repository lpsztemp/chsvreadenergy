#include <tchar.h>
#include <Windows.h>
#include "controlcodes.h"

static inline DWORD get_cpu_num() {
	SYSTEM_INFO sysinf;
	GetSystemInfo(&sysinf);
	return sysinf.dwNumberOfProcessors;
}

static DWORD cpu_num = get_cpu_num();

HANDLE OpenMsrDriver() {
	return CreateFile(_T("\\\\.\\") DRIVER_ID, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
}

const char* MsrDriverId() {
	return "\\\\.\\" DRIVER_ID_A;
}

double ReadEnergyUnitsOfCPU(HANDLE driver) {
	double units;
	DWORD dwRead;
	if (!DeviceIoControl(driver, READ_ENERGY_UNITS, NULL, 0, &units, 8, &dwRead, NULL))
		return 0.;
	return units;
}

ULONGLONG ReadEnergyOfCPU(HANDLE driver) {
	ULONGLONG energy;
	DWORD dwRead;
	if (!DeviceIoControl(driver, READ_ENERGY_STATUS, NULL, 0, &energy, 8, &dwRead, NULL))
		return 0ull;
	return energy;
}

ULONGLONG ReadMaxEnergyOfCPU(HANDLE driver) {
	ULONGLONG max_energy;
	DWORD dwRead;
	if (!DeviceIoControl(driver, READ_ENERGY_MAX, NULL, 0, &max_energy, 8, &dwRead, NULL))
		return 0ull;
	return max_energy;
}

double ReadEnergyUnitsOfCore(HANDLE driver, unsigned cpu_no) {
	double units;
	DWORD dwRead;
	if (!DeviceIoControl(driver, READ_ENERGY_UNITS_OF_CORE, &cpu_no, (DWORD) sizeof cpu_no, &units, 8, &dwRead, NULL))
		return 0.;
	return units;
}

ULONGLONG ReadEnergyOfCore(HANDLE driver, unsigned cpu_no) {
	ULONGLONG energy;
	DWORD dwRead;
	if (!DeviceIoControl(driver, READ_ENERGY_STATUS_OF_CORE, &cpu_no, (DWORD) sizeof cpu_no, &energy, 8, &dwRead, NULL))
		return 0ull;
	return energy;
}

ULONGLONG ReadMaxEnergyOfCore(HANDLE driver, unsigned cpu_no) {
	ULONGLONG max_energy;
	DWORD dwRead;
	if (!DeviceIoControl(driver, READ_ENERGY_MAX_OF_CORE, &cpu_no, (DWORD) sizeof cpu_no, &max_energy, 8, &dwRead, NULL))
		return 0ull;
	return max_energy;
}

BOOL ReadEnergyUnitsOfAllCores(HANDLE driver, double* units) {
	DWORD dwRead;
	return DeviceIoControl(driver, READ_ENERGY_UNITS_ALL_CORES, NULL, 0, units, (DWORD) (cpu_num * sizeof(double)), &dwRead, NULL);
}

BOOL ReadEnergyOfAllCores(HANDLE driver, ULONGLONG* energy) {
	DWORD dwRead;
	return DeviceIoControl(driver, READ_ENERGY_STATUS_ALL_CORES, NULL, 0, energy, (DWORD) (cpu_num * sizeof(double)), &dwRead, NULL);
}

BOOL ReadMaxEnergyOfAllCores(HANDLE driver, ULONGLONG* max_energy) {
	DWORD dwRead;
	return DeviceIoControl(driver, READ_ENERGY_MAX_ALL_CORES, NULL, 0, max_energy, (DWORD) (cpu_num * sizeof(double)), &dwRead, NULL);
}

#ifdef __cplusplus
#include <system_error>
#define RETHROWABLE_SYSTEM_ERROR(code, msg) throw std::system_error((code), std::system_category(), (msg))
#define SYSTEM_ERROR RETHROWABLE_SYSTEM_ERROR
#define TRY try {
#define RETHROW_BEGIN }catch(...) {
#define RETHROW_END throw;}
#include <cstdlib>
#include <cmath>
using std::atexit;
using std::fma;
#else
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define RETHROWABLE_SYSTEM_ERROR(code, msg) do {fprintf(stderr, "Fatal error %u: %s\n", (code), msg); goto failure;} while(0)
#define SYSTEM_ERROR(code, msg) do {fprintf(stderr, "Fatal error %u: %s\n", (code), msg); abort(__LINE__);} while(0)
#define TRY
#define RETHROW_BEGIN goto success; failure: {
#define RETHROW_END abort(__LINE__);} success:
#endif

typedef struct energy_data_ {
	HANDLE driver;
	double cpu_units;
	ULONGLONG cpu_max_energy;
	double* core_units;
	ULONGLONG *core_max_energies;
} energy_data;

static energy_data init_state();

static energy_data state = init_state();

static void close_state(energy_data* st) {
	if (st->driver != INVALID_HANDLE_VALUE) {
		CloseHandle(st->driver);
		st->driver = INVALID_HANDLE_VALUE;
	}
	if (!!st->core_units) {
		free(st->core_units);
		st->core_units = NULL;
	}
	if (!!st->core_max_energies) {
		free(st->core_max_energies);
		st->core_max_energies = NULL;
	}
}

void close_global_state() {
	close_state(&state);
}

static energy_data init_state() {
	energy_data state_ = {INVALID_HANDLE_VALUE, NULL, NULL};
	TRY
		state_.driver = OpenMsrDriver();
		if (state_.driver == INVALID_HANDLE_VALUE)
			RETHROWABLE_SYSTEM_ERROR(GetLastError(), "failed to open the energy kernel driver");
		state_.core_units = (double*) malloc(cpu_num * sizeof(double));
		if (!state_.core_units)
			RETHROWABLE_SYSTEM_ERROR(ERROR_NOT_ENOUGH_MEMORY, "failed to allocate memory for energy units");
		if (!ReadEnergyUnitsOfAllCores(state_.driver, state_.core_units))
			RETHROWABLE_SYSTEM_ERROR(GetLastError(), "failed to core read energy units from driver");
		state_.core_max_energies = (ULONGLONG*) malloc(cpu_num * sizeof(ULONGLONG));
		if (!state_.core_max_energies)
			RETHROWABLE_SYSTEM_ERROR(ERROR_NOT_ENOUGH_MEMORY, "failed to allocate memory for energy max values");
		if (!ReadMaxEnergyOfAllCores(state_.driver, state_.core_max_energies))
			RETHROWABLE_SYSTEM_ERROR(GetLastError(), "failed to read core max values of energy from driver");
		state_.cpu_units = ReadEnergyUnitsOfCPU(state_.driver);
		if (!state_.cpu_units)
			RETHROWABLE_SYSTEM_ERROR(GetLastError(), "failed to read energy units of CPU from driver");
		state_.cpu_max_energy = ReadMaxEnergyOfCPU(state_.driver);
		if (!state_.cpu_max_energy)
			RETHROWABLE_SYSTEM_ERROR(GetLastError(), "failed to max read energy of CPU from driver");
	RETHROW_BEGIN
		close_state(&state_);
	RETHROW_END
	atexit(close_global_state);
	return state_;
}

#define energy_diff_(e1, e2, emax, units) (((double) ((e2) >= (e1)?(e2) - (e1):(e2) - (e1) + (emax))) * units)

#include "readenergy.h"

energy_mark_t energy_read_core(unsigned core_index) {
	energy_mark_t result = ReadEnergyOfCore(state.driver, core_index);
	if (!result) {
		if (GetLastError() == ERROR_INVALID_PARAMETER)
			return 0ull;
		else
			SYSTEM_ERROR(GetLastError(), "failed to read energy of core");
	}
	return result;
}

double energy_diff_core(unsigned core_index, energy_mark_t en1, energy_mark_t en2) {
	return energy_diff_(en1, en2, state.core_max_energies[core_index], state.core_units[core_index]);
}

void energy_read_all_cores(energy_mark_t* en) {
	if (!ReadEnergyOfAllCores(state.driver, en))
		SYSTEM_ERROR(GetLastError(), "failed to read energy of all cores");
}

void energy_diff_all_cores(double* diffs, const energy_mark_t* en1, const energy_mark_t* en2) {
	for (size_t i = 0; i < cpu_num; ++i)
		diffs[i] = energy_diff_(en1[i], en2[i], state.core_max_energies[i], state.core_units[i]);
}

double energy_diff_average_all_cores(const energy_mark_t* en1, const energy_mark_t* en2) {
	double result = 0., d = 1./cpu_num;
	for (size_t i = 0; i < cpu_num; ++i)
		result = fma(energy_diff_(en1[i], en2[i], state.core_max_energies[i], state.core_units[i]), d, result);
	return result;
}

double energy_diff_total_all_cores(const energy_mark_t* en1, const energy_mark_t* en2) {
	double result = 0., d = 1./cpu_num;
	for (size_t i = 0; i < cpu_num; ++i)
		result += energy_diff_(en1[i], en2[i], state.core_max_energies[i], state.core_units[i]);
	return result;
}

energy_mark_t energy_read_cpu() {
	return ReadEnergyOfCPU(state.driver);
}

double energy_diff_cpu(energy_mark_t en1, energy_mark_t en2) {
	return energy_diff_(en1, en2, state.cpu_max_energy, state.cpu_units);
}
