#include "readenergy.h"

#include <concepts>
#include <cstdlib>
#include <cstdint>
#include <climits>
#include <memory>
#include <omp.h>

template <std::unsigned_integral T, T modulo = 0>
struct lincon_t
{
	static_assert((modulo - 1 ^ modulo) == modulo || modulo + modulo <= sizeof modulo * CHAR_BIT);
	T multiplier = 1, addend = 0;
	constexpr lincon_t operator*(const lincon_t& r) const
	{
		if constexpr (!modulo)
			return lincon_t{r.multiplier * multiplier, r.multiplier * addend + r.addend};
		else
			return lincon_t{(r.multiplier * multiplier) % modulo, (r.multiplier * addend % modulo + r.addend) % modulo};
	}
	constexpr lincon_t& operator*=(const lincon_t& r)
	{
		addend += multiplier * r.addend;
		multiplier *= r.multiplier;
		if constexpr (!!modulo) {
			multiplier %= modulo;
			addend %= modulo;
		}
		return *this;
	}
	constexpr T operator()(T seed) const
	{
		auto result = multiplier * seed + addend;
		if constexpr (!!modulo)
			result %= modulo;
		return result;
	}
	constexpr T operator()(T seed, T min, T max) const {
		T rem = max + 1 - min, res = (*this)(seed);
		return !rem?res:res % rem + min;
	}
};

template <std::unsigned_integral T, T M>
void randomize_serial(T seed, T* pData, std::size_t N, const lincon_t<T, M>& g, T min, T max)
{
	lincon_t<T, M> myg;
	for (std::size_t i = 0; i < N; i++)
		pData[i] = (myg *= g)(seed, min, max);
}

template <class T, std::unsigned_integral U>
constexpr T pow(T val, U exp) requires requires(T x, T y) {x *= y; T(1);}
{
	auto result = T{1};
	while (!!exp)
	{
		if (!!(exp & 1))
			result *= val;
		val *= val;
		exp >>= 1;
	}
	return result;
}

template <std::unsigned_integral T, T M>
void randomize_parallel_co(T seed, T* pData, std::size_t N, const lincon_t<T, M>& g, T min, T max)
{
	int x = 0;
	#pragma omp parallel
	{
		auto numT = (std::size_t)omp_get_num_threads();
		auto t = (std::size_t)omp_get_thread_num();
		auto extra_word = N % numT;
		std::size_t B = 0;
		std::size_t it = 0;
		if (t < extra_word)
		{
			B = N / numT + 1;
			it = B * t;
		}
		else
		{
			B = N / numT;
			it = extra_word + B * t;
		}
		auto myg = pow(g, it);
		for (auto i = it; i < it + B; i++)
			pData[i] = (myg *= g)(seed, min, max);
	}
}

constexpr std::uint32_t lc_multiplier = UINT32_C(0x41c64e6d);
constexpr std::uint32_t lc_addend = UINT32_C(12345);
constexpr std::uint32_t lc_modulo = UINT32_C(1) << 31;

void randomize_serial(std::uint32_t seed, std::uint32_t* pData, std::size_t N, std::uint32_t min, std::uint32_t max) {
	randomize_serial(seed, pData, N, lincon_t<std::uint32_t, lc_modulo>{lc_multiplier, lc_addend}, min, max);
}

void randomize_parallel_co(std::uint32_t seed, std::uint32_t* pData, std::size_t N, uint32_t min, uint32_t max) {
	randomize_parallel_co(seed, pData, N, lincon_t<std::uint32_t, lc_modulo>{lc_multiplier, lc_addend}, min, max);
}

#include <iostream>
#include <stdexcept>
#include <vector>

struct energy_datum {
	std::size_t byte_size;
	double sequential, parallel;
};

auto energy_test() {
	constexpr std::size_t sample_min = 1u << 18, sample_max = 1u << 24, sample_step = 1u << 18;
	std::vector<energy_datum> results;
	auto data_sample = std::make_unique<uint32_t[]>(sample_max);
	for (std::size_t cw = sample_min; cw <= sample_max; cw += sample_step) {
		energy_datum result;
		energy_mark_t e1, e2;
		result.byte_size = cw << 2;
		e1 = energy_read_cpu();
		randomize_serial((uint32_t) cw, data_sample.get(), cw, 0, UINT32_MAX);
		e2 = energy_read_cpu();
		result.sequential = energy_diff_cpu(e1, e2);
		e1 = energy_read_cpu();
		randomize_parallel_co((uint32_t) cw + 1, data_sample.get(), cw, 0, UINT32_MAX);
		e2 = energy_read_cpu();
		result.parallel = energy_diff_cpu(e1, e2);
		results.push_back(result);
	}
	return results;
}

int main(int arg, char** argv) {
	try {
		auto results = energy_test();
		std::cout << "Size,Sequential,Parallel\n";
		for (auto result:results)
			std::cout << result.byte_size << ',' << result.sequential << ',' << result.parallel << '\n';
	}catch (std::exception& ex) {
		std::cerr << "Exception: " << ex.what() << '\n';
		return -1;
	}
	return 0;
}
