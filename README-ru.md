[![en](https://img.shields.io/badge/language-english-red.svg)](https://gitlab.com/lpsztemp/chsvreadenergy/-/blob/main/README.md)


Репозиторий содержит реализацию драйвера для Windows в режиме ядра, который считывает данные об энергопотреблении процессора через регистры Intel RAPL, специфичные для модели. Этот доступ к данным об энергии в настоящее время возможен только из кода ядра, поэтому драйвер данного репозитория работает в режиме ядра.

Кроме того, репозиторий содержит демонстрационный проект, который использует драйвер для оценки энергопотребления процессоров при выполнении последовательных и параллельных рандомизаторов для буферов памяти.

# Компиляция для Windows

Для компиляции драйвера используйте **Windows Driver Kit** версии, соответствующей целевой версии Windows.

Решение для Visual Studio предоставляет следующие конфигурации сборки:

Конфигурация               | Целевая Windows
----------------------------|---------------------------------------
win8.1-debug/win8.1-release | 8.1 and Server 2012 R2
win10-debug/win10-release   | 10, 11, Server 2016, Server 2019 and Server 2022

Поддерживается только архитектура процессоров x86-64 и только процессоры Intel, поэтому единственная доступная целевая платформа для Visual Studio — это "x64".

# Установка на Windows

Код из репозитория использует сертификат тестовой подписи по умолчанию для подписания драйвера, поэтому его установка на Windows требует следующего.
* Отключите Secure Boot в BIOS.
* В терминале Windows, запущенном с правами администратора, выполните следующие команды:
	* ``bcdedit /debug on`` для включения режима отладки ядра;
	* ``bcdedit /set nointegritychecks off`` для отключения проверки целостности драйвера;
	* ``bcdedit /set testsigning on`` для разрешения установки драйверов с тестовой подписью;
	* ``shutdown /r /t 0`` для перезагрузки компьютера.
  
* После перезагрузки используйте **devcon.exe** для установки, обновления или удаления драйвера. Программа **devcon.exe** является частью [Windows Driver Kit] и обычно находится в ``<путь к WDK>\Tools\x64`` где ``<путь к WDK>`` это директория с установленным набором инструментов. Например, путь по умолчанию для Windows Driver Kit на Windows 10 — это ``C:\Program Files (x86)\Windows Kits\10``, а путь к **devcon.exe** - ``C:\Program Files (x86)\Windows Kits\10\Tools\x64\devcon.exe``.
Программа **devcon.exe** является портативной и не требует установки, то есть файл **devcon.exe** можно перемещать между целевыми компьютерами для использования на любой версии Windows, перечисленной в таблице выше. 
Также см. [этот пост на StackExchange](https://superuser.com/a/1099688).  
Предположим, что ``<path-to-driver>`` — это директория, в которой находятся три файла драйвера: **chsvreaderenergy.cat**, **chsviewreaderenergy.inf** и **chsviewreaderenergy.sys**. Например, результат сборки по умолчанию в конфигурации ``win10-release``, то есть эти три файла, расположены в ``<repo>\x64\win10-release``, где ``<repo>`` — это локальный путь к репозиторию.
	* Используйте ``devcon.exe install <path-to-driver>\chsvreadenergy.inf Root\chsvreadenergy`` для установки драйвера.
	* Используйте ``devcon.exe update <path-to-driver>\chsvreadenergy.inf Root\chsvreadenergy`` для обновления существующей установки до новой версии.
	* Используйте ``devcon.exe remove Root\chsvreadenergy`` для удаления драйвера.
  
Обратите внимание, что эти команды чувствительны к используемому символу слеша. Программа **devcon.exe** может дать сбой, если прямой слэш `/` используется вместо обратного `\`. Также обратите внимание, что после обновления или удаления драйвера может потребоваться перезагрузка системы.

# Платформы, протестированные с драйвером

На данный момент драйвер был протестирован в следующих конфигурациях:
* Intel Xeon E5-2695 v2 (06_3EH), 2-CPU configuration, on Windows Server 2012 R2;
* Intel i9-10980XE (06_55H) on Windows 10 22H2 and Windows 11 22H2;
* Intel i7-7700HQ (06_9EH) on Windows 10 22H2;
* Intel i7-4700HQ (06_3CH) on Windows 8.1.

[Windows Driver Kit]: https://learn.microsoft.com/en-us/windows-hardware/drivers/other-wdk-downloads