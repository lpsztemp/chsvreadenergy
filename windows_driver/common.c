#include <ntddk.h>
#include <intrin.h>

void ReadMsrDeferred(PKDPC dpc, void* deferred, void* arg1, void* arg2) {
	#pragma warning(push)
	#pragma warning(disable: 4311)
	ULONG msr = (ULONG) deferred;
	#pragma warning(pop)
	ULONGLONG* output = arg1;
	volatile LONG* complete = arg2;
	dpc;

	*output = __readmsr(msr);
	InterlockedDecrement(complete);
}

NTSTATUS ReadCoreMsr(unsigned cpu_index, ULONG msr, ULONGLONG* result_nonpaged) {
	PROCESSOR_NUMBER cpu_num;
	KIRQL irql;
	volatile LONG completeness;
	KDPC dpc;
	NTSTATUS result_code = KeGetProcessorNumberFromIndex(cpu_index, &cpu_num);
	if (!!result_code)
		return result_code;
	completeness = 1;
	irql = KeGetCurrentIrql();
	if (irql < DISPATCH_LEVEL)
		KeRaiseIrqlToDpcLevel();
	__try {
		KeInitializeDpc(&dpc, ReadMsrDeferred, (PVOID) msr);
		KeSetTargetProcessorDpcEx(&dpc, &cpu_num);
		KeInsertQueueDpc(&dpc, result_nonpaged, (PVOID) &completeness);
	}__finally {
		if (irql < DISPATCH_LEVEL)
			KeLowerIrql(irql);
	}
	while (!!completeness)
		continue;
	return STATUS_SUCCESS;
}
