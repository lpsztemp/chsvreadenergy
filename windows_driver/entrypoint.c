#include <stddef.h>
#include <ntddk.h>
#include <stdbool.h>
#include "controlcodes.h"
#include <intrin.h>

static NTSTATUS (*OnDeviceControl) (PDEVICE_OBJECT pDevice, PIRP pIrp);
NTSTATUS OnDeviceControlIntel(PDEVICE_OBJECT pDO, PIRP pIrp);
NTSTATUS OnDeviceControlAmd(PDEVICE_OBJECT pDO, PIRP pIrp);

static NTSTATUS (*OnRead) (PDEVICE_OBJECT pDO, PIRP pIrp);
NTSTATUS OnReadIntel(PDEVICE_OBJECT pDO, PIRP pIrp);
NTSTATUS OnReadAmd(PDEVICE_OBJECT pDO, PIRP pIrp);

inline static bool energy_measurement_support() {
	int id_data[4];
	unsigned __int64 efl = __readeflags();
	__writeeflags(efl ^= (1ull << 21));
	efl ^= __readeflags();
	if (!!efl) //CPUID
		return false;
	__cpuid(id_data, 0);
	if (id_data[0] < 1) //CPUID basic level
		return false;
	if (id_data[1] == 0x756e6547 && id_data[2] == 0x6c65746e && id_data[3] == 0x49656e69) { //GenuineIntel
		__cpuid(id_data, 1);
		if (!(id_data[3] & 5)) //MSR
			return false;
		if (!!(id_data[0] & ((~6ull & 0xFull) << 8)))
			return false;
		id_data[0] &= 0xF00F0;
		switch (id_data[0]) //INTEL_MSR_PP0_ENERGY_STATUS, MSR_PKG_ENERGY_STATUS and MSR_RAPL_POWER_UNIT are both supported and have the offsets of 0x639, 0x611 and 0x606 respectively
		{
			case 0x200A0:
			case 0x200D0:
			case 0x30070:
			case 0x300A0:
			case 0x300C0:
			case 0x300E0:
			case 0x300F0:
			case 0x40050:
			case 0x40060:
			case 0x400A0:
			case 0x400D0:
			case 0x400F0:
			case 0x50050: //
			case 0x50060:
			case 0x50070:
			case 0x500A0:
			case 0x500C0:
			case 0x500D0:
			case 0x700A0:
			case 0x80050:
			case 0x900E0: //
				OnDeviceControl = OnDeviceControlIntel;
				OnRead = OnReadIntel;
				return true;
		}
		return false;
	}else if (id_data[1] == 0x68747541 && id_data[2] == 0x444D4163 && id_data[4] == 0x69746E65) { //AuthenticAMD
		__cpuid(id_data, 1);
		if (!(id_data[3] & 5)) //MSR
			return false;
		__cpuidex(id_data, 0x80000000, 0);
		if (id_data[0] < 7)
			return false;
		__cpuidex(id_data, 0x80000007, 0);
		if (!(id_data[2] & ~0ul)) //CpuPwrSampleTimeRatio
			return false;
		if (id_data[0] < 7)
			return false;
		OnDeviceControl = OnDeviceControlAmd;
		OnRead = OnReadAmd;
		return false; //UNTESTED
	}else {
		return false;
	}
}

static NTSTATUS OnCreate(PDEVICE_OBJECT pDO, PIRP pIrp) {
	pDO;
	pIrp->IoStatus.Information = 0;
	pIrp->IoStatus.Status = STATUS_SUCCESS;
	IoCompleteRequest(pIrp, IO_NO_INCREMENT);
	return STATUS_SUCCESS;
}

static NTSTATUS OnClose(PDEVICE_OBJECT pDO, PIRP pIrp) {
	pDO;
	pIrp->IoStatus.Information = 0;
	pIrp->IoStatus.Status = STATUS_SUCCESS;
	IoCompleteRequest(pIrp, IO_NO_INCREMENT);
	return STATUS_SUCCESS;
}

static void Unload(PDRIVER_OBJECT pDO) {
	UNICODE_STRING device_name = RTL_CONSTANT_STRING(DOS_DEVICE_NAME);
	PAGED_CODE();
	IoDeleteSymbolicLink(&device_name);
	if (pDO->DeviceObject != NULL)
		IoDeleteDevice(pDO->DeviceObject);
}

NTSTATUS DriverEntry(PDRIVER_OBJECT pDO, UNICODE_STRING reg) {
	UNICODE_STRING nt_name = RTL_CONSTANT_STRING(NT_DEVICE_NAME), dos_name = RTL_CONSTANT_STRING(DOS_DEVICE_NAME);
	PDEVICE_OBJECT device;
	NTSTATUS err;
	if (!energy_measurement_support())
		return STATUS_NOT_IMPLEMENTED;
	err = IoCreateDevice(pDO, 0, &nt_name, CHSV_READENERGY_TYPE, FILE_READ_ONLY_DEVICE, FALSE, &device);
	reg;
	if (!NT_SUCCESS(err))
		return err;
	pDO->MajorFunction[IRP_MJ_CREATE] = OnCreate;
	pDO->MajorFunction[IRP_MJ_READ] = OnRead;
	//pDO->MajorFunction[IRP_MJ_CLOSE] = OnClose;
	pDO->MajorFunction[IRP_MJ_DEVICE_CONTROL] = OnDeviceControl;
	pDO->DriverUnload = Unload;
	err = IoCreateSymbolicLink(&dos_name, &nt_name);
	if (!NT_SUCCESS(err))
		IoDeleteDevice(device);
	return err;
}
