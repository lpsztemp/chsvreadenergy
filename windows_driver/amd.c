#include <ntddk.h>
#include "controlcodes.h"
#include <intrin.h>

void ReadMsrDeferred(PKDPC dpc, void* deferred, void* arg1, void* arg2);
NTSTATUS ReadCoreMsr(unsigned cpu_index, ULONG msr, ULONGLONG* result_nonpaged);

// https://docs.kernel.org/hwmon/fam15h_power.html
#define AMD_MSR_CpuSwPwrAcc 0xC001007A
#define AMD_MSR_MaxCpuSwPwrAcc 0xC001007B
//https://me.sakana.moe/2023/09/06/measuring-cpu-power-consumption/
#define AMD_MSR_PWR_UNIT 0xC0010299
#define AMD_MSR_CORE_ENERGY 0xC001029A
#define AMD_MSR_PACKAGE_ENERGY 0xC001029B

static NTSTATUS IrpReadAmdMsr(PIRP pIrp, ULONG msr) {
	__try {
		ULONG output_size = pIrp->MdlAddress->ByteCount;
		ULONGLONG* output_buffer = (ULONGLONG*) ((char*) pIrp->MdlAddress->StartVa + pIrp->MdlAddress->ByteOffset);
		if (output_size != sizeof(ULONGLONG))
			return output_size < sizeof(ULONGLONG)?STATUS_BUFFER_TOO_SMALL:STATUS_BUFFER_OVERFLOW;
		*output_buffer = !!msr?__readmsr(msr):1023ull << 52;
		pIrp->IoStatus.Information = sizeof(ULONGLONG);
		return STATUS_SUCCESS;
	}__except (GetExceptionCode() == STATUS_ACCESS_VIOLATION?EXCEPTION_EXECUTE_HANDLER:EXCEPTION_CONTINUE_SEARCH) {
		return STATUS_ACCESS_VIOLATION;
	}
}

static NTSTATUS IrpReadAmdCoreMsr(PIRP pIrp, PIO_STACK_LOCATION pIrpStack, ULONG msr) {
	unsigned* input_buffer = pIrpStack->Parameters.DeviceIoControl.Type3InputBuffer;
	ULONG input_size = pIrpStack->Parameters.DeviceIoControl.InputBufferLength;
	ULONGLONG* output_buffer = pIrp->UserBuffer;
	ULONG output_size = pIrpStack->Parameters.DeviceIoControl.OutputBufferLength;
	NTSTATUS result_code;
	unsigned cpu_index;
	if (input_size != sizeof cpu_index)
		return input_size < sizeof cpu_index?STATUS_BUFFER_TOO_SMALL:STATUS_BUFFER_OVERFLOW;
	if (output_size != sizeof(ULONGLONG)) 
		return output_size < sizeof(ULONGLONG)?STATUS_BUFFER_TOO_SMALL:STATUS_BUFFER_OVERFLOW;
	__try {
		cpu_index = *input_buffer;
		if (!msr)  {
			PROCESSOR_NUMBER cpu_num;
			result_code = KeGetProcessorNumberFromIndex(cpu_index, &cpu_num);
			if (!!result_code)
				return result_code;
			*output_buffer = 1023ull << 52;
		}else {
			ULONGLONG result_nonpaged;
			cpu_index = *input_buffer;
			ProbeForWrite(output_buffer, sizeof(ULONGLONG), 1);
			result_code = ReadCoreMsr(cpu_index, msr, &result_nonpaged);
			if (!!result_code)
				return result_code;
			*output_buffer = result_nonpaged;
		}
	}__except (GetExceptionCode() == STATUS_ACCESS_VIOLATION?EXCEPTION_EXECUTE_HANDLER:EXCEPTION_CONTINUE_SEARCH) {
		return STATUS_ACCESS_VIOLATION;
	}
	pIrp->IoStatus.Information = sizeof(ULONGLONG);
	return STATUS_SUCCESS;
}

#pragma warning(push)
#pragma warning(disable:4996)
static NTSTATUS IrpReadAllAmdCoresMsr(PIRP pIrp, ULONG msr) {
	ULONG cpus = KeQueryActiveProcessorCount(NULL);
	ULONGLONG* output_buffer = (ULONGLONG*) ((char*) pIrp->MdlAddress->StartVa + pIrp->MdlAddress->ByteOffset);
	ULONG output_size = pIrp->MdlAddress->ByteCount;
	if (output_size != cpus * sizeof(ULONGLONG)) 
		return output_size < cpus * sizeof(ULONGLONG)?STATUS_BUFFER_TOO_SMALL:STATUS_BUFFER_OVERFLOW;
	__try {
		if (!msr) {
			for (size_t cpu = 0; cpu < cpus; ++cpu)
				output_buffer[cpu] = 1023ull << 52;
		}else {
			volatile LONG completeness;
			ULONGLONG* nonpaged_results;
			ProbeForWrite(output_buffer, output_size, 1);
			nonpaged_results = ExAllocatePool(NonPagedPool, cpus * sizeof(ULONGLONG));
			if (!nonpaged_results)
				return STATUS_NO_MEMORY;
			__try {
				KIRQL irql;
				PKDPC dpcs = ExAllocatePool(NonPagedPool, cpus * sizeof(KDPC));
				if (!dpcs)
					return STATUS_NO_MEMORY;
				__try {
					completeness = (LONG) cpus;
					irql = KeGetCurrentIrql();
					if (irql < DISPATCH_LEVEL)
						KeRaiseIrqlToDpcLevel();
					__try {
						PROCESSOR_NUMBER cpu_num;
						for (unsigned long cpu = 0; cpu < cpus; ++cpu) {
							if (!!KeGetProcessorNumberFromIndex(cpu, &cpu_num))
								nonpaged_results[cpu] = 0ull;
							else {
								KeInitializeDpc(&dpcs[cpu], ReadMsrDeferred, (PVOID) msr);
								KeSetTargetProcessorDpcEx(&dpcs[cpu], &cpu_num);
								KeInsertQueueDpc(&dpcs[cpu], &nonpaged_results[cpu], (PVOID) &completeness);
							}
						}
					}__finally {
						if (irql < DISPATCH_LEVEL)
							KeLowerIrql(irql);
					}
					while (!!completeness)
						continue;
				}__finally {
					ExFreePool(dpcs);
				}
				memcpy(output_buffer, nonpaged_results, cpus * sizeof(ULONGLONG));
			}__finally {
				ExFreePool(nonpaged_results);
			}
		}
	}__except (GetExceptionCode() == STATUS_ACCESS_VIOLATION?EXCEPTION_EXECUTE_HANDLER:EXCEPTION_CONTINUE_SEARCH) {
		return STATUS_ACCESS_VIOLATION;
	}
	pIrp->IoStatus.Information = cpus * sizeof(ULONGLONG);
	return STATUS_SUCCESS;
}
#pragma warning(pop)

NTSTATUS OnDeviceControlAmd(PDEVICE_OBJECT pDO, PIRP pIrp) {
	PIO_STACK_LOCATION pIrpStack;
	NTSTATUS result_code = STATUS_SUCCESS;
	pDO;

	pIrpStack = IoGetCurrentIrpStackLocation(pIrp);

	NT_ASSERT(pIrpStack->MajorFunction == IRP_MJ_DEVICE_CONTROL);
	switch (pIrpStack->Parameters.DeviceIoControl.IoControlCode)
	{
	case READ_ENERGY_STATUS:
		result_code = IrpReadAmdMsr(pIrp, AMD_MSR_CpuSwPwrAcc);
		break;
	case READ_ENERGY_UNITS:
		result_code = IrpReadAmdMsr(pIrp, 0);
		break;
	case READ_ENERGY_MAX:
		result_code = IrpReadAmdMsr(pIrp, AMD_MSR_MaxCpuSwPwrAcc);
		break;
	case READ_ENERGY_STATUS_OF_CORE:
		result_code = IrpReadAmdCoreMsr(pIrp, pIrpStack, AMD_MSR_CpuSwPwrAcc);
		break;
	case READ_ENERGY_UNITS_OF_CORE:
		result_code = IrpReadAmdCoreMsr(pIrp, pIrpStack, 0);
		break;
	case READ_ENERGY_MAX_OF_CORE:
		result_code = IrpReadAmdCoreMsr(pIrp, pIrpStack, AMD_MSR_MaxCpuSwPwrAcc);
		break;
	case READ_ENERGY_STATUS_ALL_CORES:
		result_code = IrpReadAllAmdCoresMsr(pIrp, AMD_MSR_CpuSwPwrAcc);
		break;
	case READ_ENERGY_UNITS_ALL_CORES:
		result_code = IrpReadAllAmdCoresMsr(pIrp, 0);
		break;
	case READ_ENERGY_MAX_ALL_CORES:
		result_code = IrpReadAllAmdCoresMsr(pIrp, AMD_MSR_MaxCpuSwPwrAcc);
		break;
	default:
		result_code = STATUS_NOT_IMPLEMENTED;
	};
	if (!!result_code)
		pIrp->IoStatus.Information = 0;
	pIrp->IoStatus.Status = result_code;
	IoCompleteRequest(pIrp, IO_NO_INCREMENT);
	return result_code;
}

NTSTATUS OnReadAmd(PDEVICE_OBJECT pDO, PIRP pIrp) {
	PIO_STACK_LOCATION pIrpStack = IoGetCurrentIrpStackLocation(pIrp);
	NTSTATUS result = STATUS_SUCCESS;
	pDO;
	if (pIrpStack->Parameters.Read.Length != sizeof(ULONGLONG))
		result = STATUS_INVALID_PARAMETER;
	else __try {
		switch (pIrpStack->Parameters.Read.ByteOffset.QuadPart) {
		case AMD_MSR_CpuSwPwrAcc:
		case AMD_MSR_MaxCpuSwPwrAcc:
		case AMD_MSR_PWR_UNIT:
		case AMD_MSR_CORE_ENERGY:
		case AMD_MSR_PACKAGE_ENERGY:
			*(PULONGLONG) pIrp->UserBuffer = __readmsr(pIrpStack->Parameters.Read.ByteOffset.LowPart);
			pIrp->IoStatus.Information = sizeof(ULONGLONG);
			break;
		default:
			result = STATUS_ACCESS_DENIED;
		}
	} __except (GetExceptionCode() == STATUS_ACCESS_VIOLATION?EXCEPTION_EXECUTE_HANDLER:EXCEPTION_CONTINUE_SEARCH) {
		result = STATUS_ACCESS_VIOLATION;
	}
	if (!!result)
		pIrp->IoStatus.Information = 0;
	pIrp->IoStatus.Status = result;
	IoCompleteRequest(pIrp, IO_NO_INCREMENT);
	return result;
}